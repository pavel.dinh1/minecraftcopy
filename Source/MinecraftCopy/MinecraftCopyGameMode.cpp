// Copyright Epic Games, Inc. All Rights Reserved.

#include "MinecraftCopyGameMode.h"
#include "MinecraftCopyHUD.h"
#include "MinecraftCopyCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMinecraftCopyGameMode::AMinecraftCopyGameMode()
	: Super()
{
	// use our custom HUD class
	HUDClass = AMinecraftCopyHUD::StaticClass();
}
