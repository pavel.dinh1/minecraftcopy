// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MinecraftCopyGameMode.generated.h"

UCLASS(minimalapi)
class AMinecraftCopyGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMinecraftCopyGameMode();
};



