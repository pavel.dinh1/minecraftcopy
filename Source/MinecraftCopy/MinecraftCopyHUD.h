// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MinecraftCopyHUD.generated.h"

UCLASS()
class AMinecraftCopyHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMinecraftCopyHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

