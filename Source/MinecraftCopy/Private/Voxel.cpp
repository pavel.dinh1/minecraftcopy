// Fill out your copyright notice in the Description page of Project Settings.


#include "Voxel.h"
#include "ProceduralMeshComponent.h"

const int32 bTriangles[] = { 2,1,0,0,3,2 };

const FVector2D bUVs[] = { FVector2D(0.f, 0.f), FVector2D(0.f,1.f), FVector2D(1.f,1.f), FVector2D(1.f,0.f) };

// Top
const FVector bNormals0[] = { FVector(0.f,0.f,1.f), FVector(0.f,0.f,1.f), FVector(0.f,0.f,1.f), FVector(0.f,0.f,1.f) };

// Bottom
const FVector bNormals1[] = { FVector(0.f,0.f,-1.f), FVector(0.f,0.f,-1.f), FVector(0.f,0.f,-1.f), FVector(0.f,0.f,-1.f) };

// Front
const FVector bNormals2[] = { FVector(0.f,1.f,0.f), FVector(0.f,1.f,0.f), FVector(0.f,1.f,0.f), FVector(0.f,1.f,0.f), };

// Back
const FVector bNormals3[] = { FVector(0.f,-1.f,0.f), FVector(0.f,-1.f,0.f), FVector(0.f,-1.f,0.f), FVector(0.f,-1.f,0.f) };

// Right
const FVector bNormals4[] = { FVector(1.f,0.f,0.f), FVector(1.f,0.f,0.f), FVector(1.f,0.f,0.f), FVector(1.f,0.f,0.f) };

// Left
const FVector bNormals5[] = { FVector(-1.f,0.f,0.f), FVector(-1.f,0.f,0.f), FVector(-1.f,0.f,0.f), FVector(-1.f,0.f,0.f) };

// Need to to check surrounding voxels
const FVector bMask[] = { FVector(0.f,0.f,1.f), FVector(0.f,0.f,-1.f), FVector(0.f,1.f,0.f), FVector(0.f,-1.f,0.f), FVector(1.f,0.f,0.f),FVector(-1.f,0.f,0.f) };


// Sets default values
AVoxel::AVoxel()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AVoxel::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AVoxel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AVoxel::OnConstruction(const FTransform& Transform)
{
	ChunkZElements = 80;
	ChunkTotalElements = ChunkLineElements * ChunkLineElements * ChunkZElements;
	ChunkLineElementsSquared = ChunkLineElements * ChunkLineElements;
	VoxelSizeHalf = VoxelSize / 2;

	FString String = "Voxel_" + FString::FromInt(ChunkXIndex) + "_" + FString::FromInt(ChunkYIndex);
	FName Name = FName(*String);

	ProceduralComponent = NewObject<UProceduralMeshComponent>(this, Name);
	RootComponent = ProceduralComponent;

	RootComponent->SetWorldTransform(Transform); // Set position of root component (bubble actor)

	Super::OnConstruction(Transform);

	GenerateChunk();
	UpdateMesh();
}

void AVoxel::GenerateChunk()
{
	ChunkFields.SetNumUninitialized(ChunkTotalElements);

	for (int32 x = 0; x < ChunkLineElements; x++)
	{
		for (int32 y = 0; y < ChunkLineElements; y++)
		{
			for (int32 z = 0; z < ChunkZElements; z++)
			{
				int32 index = x + (y * ChunkLineElements) + (z * ChunkLineElementsSquared);
				ChunkFields[index] = (z < 30) ? 1 : 0;
			}
		}
	}
}

void AVoxel::UpdateMesh()
{
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FProcMeshTangent> Tangents;
	TArray<FColor> VertexColors;
	int32 ElementID = 0;


	for (int32 x = 0; x < ChunkLineElements; x++)
	{
		for (int32 y = 0; y < ChunkLineElements; y++)
		{
			for (int32 z = 0; z < ChunkZElements; z++)
			{
				int32 index = x + (ChunkLineElements * y) + (ChunkLineElementsSquared * z);
				int32 MeshIndex = ChunkFields[index];

				if (MeshIndex > 0)
				{
					MeshIndex--;

					// Add faces, vertices, UVs, and Normals
					int TriangleNum = 0;
					for (int32 i = 0; i < 6; i++)
					{
						int NewIndex = index + bMask[i].X + (bMask[i].Y * ChunkLineElements) + (bMask[i].Z * ChunkLineElementsSquared);

						bool Flag = false;
						if (MeshIndex >= 20) Flag = true;
						else if ((x + bMask[i].X < ChunkLineElements) && (x + bMask[i].X >= 0) && (y + bMask[i].Y < ChunkLineElements) && (y + bMask[i].Y >= 0))
						{
							if (NewIndex < ChunkFields.Num() && NewIndex >= 0)
								if (ChunkFields[NewIndex] < 1) Flag = true;
						}
						else Flag = true;

						if (Flag)
						{
							Triangles.Add(bTriangles[0] + TriangleNum + ElementID);
							Triangles.Add(bTriangles[1] + TriangleNum + ElementID);
							Triangles.Add(bTriangles[2] + TriangleNum + ElementID);
							Triangles.Add(bTriangles[3] + TriangleNum + ElementID);
							Triangles.Add(bTriangles[4] + TriangleNum + ElementID);
							Triangles.Add(bTriangles[5] + TriangleNum + ElementID);
							TriangleNum += 4; // add 4 vertices to next triangle
						}

						// Faces
						switch (i)
						{
						case 0:
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));

							Normals.Append(bNormals0, UE_ARRAY_COUNT(bNormals0));
							break;

						case 1:
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));

							Normals.Append(bNormals1, UE_ARRAY_COUNT(bNormals1));
							break;

						case 2:
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));

							Normals.Append(bNormals2, UE_ARRAY_COUNT(bNormals2));
							break;

						case 3:
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));

							Normals.Append(bNormals3, UE_ARRAY_COUNT(bNormals3));
							break;

						case 4:
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));

							Normals.Append(bNormals5, UE_ARRAY_COUNT(bNormals4));
							break;
						case 5:
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), -VoxelSizeHalf + (z * VoxelSize)));
							Vertices.Add(FVector(-VoxelSizeHalf + (x * VoxelSize), -VoxelSizeHalf + (y * VoxelSize), VoxelSizeHalf + (z * VoxelSize)));

							Normals.Append(bNormals4, UE_ARRAY_COUNT(bNormals5));
							break;
						}
						UVs.Append(bUVs, UE_ARRAY_COUNT(bUVs));
						FColor Color = FColor(255, 255, 255, i);

						VertexColors.Add(Color);
						VertexColors.Add(Color);
						VertexColors.Add(Color);
						VertexColors.Add(Color);
					}
					ElementID += TriangleNum;
				}
			}
		}
	}
	ProceduralComponent->ClearAllMeshSections();
	ProceduralComponent->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, VertexColors, Tangents, true);
}
