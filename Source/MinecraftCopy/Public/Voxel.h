// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Voxel.generated.h"

UCLASS()
class MINECRAFTCOPY_API AVoxel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVoxel();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UMaterialInterface *> Materials; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true))
	int32 RandomSeed = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true))
	int32 VoxelSize = 200;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true))
	int32 ChunkLineElements = 10;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true))
	int32 ChunkXIndex = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true))
	int32 ChunkYIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float XMult = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float YMult = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ZMult = 1.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Weight = 1.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Freq = 1.f;

	UPROPERTY()
	int32 ChunkTotalElements;
	
	UPROPERTY()
	int32 ChunkZElements;
	
	UPROPERTY()
	int32 ChunkLineElementsSquared;

	UPROPERTY()
	int32 VoxelSizeHalf;

	UPROPERTY()
	TArray<int32> ChunkFields;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UProceduralMeshComponent* ProceduralComponent;

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	void GenerateChunk();

	void UpdateMesh();
};
